# Ornate KWGT Wallpapers

Wallpaper storage for [Ornate KWGT](https://play.google.com/store/apps/details?id=ornatekwgt.kustom.pack) app.

## Wallpaper Upload Guidelines

It is recommended to follow these guidelines while uploading new wallpapers. This makes it easier to edit the JSON file.

- Use photo caption as file name, replacing all spaces with underscores. If your photo caption is "Photo Caption", then the file name will be `Photo_Caption` with proper extension like `.png.`, `.jpg`, `.jpeg`, etc.
- Thumbnails should have `_thumb` following the file name of the full resolution wallpapers. If the file name of the full resolution wallpaper is `Photo_Caption.jpg`, then the thumbnail should have `Photo_Caption_thumb.jpg` as the file name.